# Project

# Operating system

This project should work on Linux Debian-like distributions, e.g., Ubuntu 20.04.


## Install project dependencies

Run:
```
./first-time-install.sh

```


## Python subproject

### Setup python environment

Run:
```
cd python
make set-py-venv-with-deps
```

### Activate python virtual env

Run:
```
cd python
source .env/bin/activate
```

### Deactivate python virtual env

Run:
```
cd python
deactivate
```

### Reformat code style

Run:
```
cd python
make style
```

### Clean env

Run:
```
cd python
make clean
```


## Rust subproject

TODO: Implement methods in Rust.
