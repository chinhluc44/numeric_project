use core::f64::consts::E;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::Write;
use std::path::Path;

use rust_methods::methods::benchmark::{current_time_nanos, record_execution_times};
use rust_methods::methods::error_type::ErrorType::Absolute;
use rust_methods::methods::factorization_solvers::solve_by_crout_fac;
use rust_methods::methods::false_rule::false_rule;
use rust_methods::methods::gaussian_elim_with_partial_pivot::solve_by_gaussian_elim_with_partial_pivot;
use rust_methods::methods::gaussian_elim_with_total_pivot::solve_by_gaussian_elim_with_total_pivot;
use rust_methods::methods::iterative_methods::seidel_method;
use rust_methods::methods::matrix_utils::read_data;
use rust_methods::methods::multiple_roots::multiple_roots;
use rust_methods::methods::newton::newton;
use rust_methods::methods::newton_interpolation::newton_interpolation;
use rust_methods::methods::regressive_substitution::regressive_substitution;
use rust_methods::methods::secant::secant;
use rust_methods::methods::simple_gaussian_elim::solve_by_simple_gaussian_elim;
use rust_methods::methods::splines::quadratic_spline;

fn main() {
    let mut results: HashMap<String, Vec<(usize, u128)>> = HashMap::new();

    let execs = 55;
    let tol = 0.5 * (10_f64).powf(-12.);
    let f0 = |x: f64| f64::cos(x / 10.);

    let t = record_execution_times(execs, || {
        let t0 = current_time_nanos();
        false_rule(f0, (460., 510.), tol, Absolute);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("false_rule".to_string(), t);

    let f1 = |x: f64| return x.powf(5.) - x.powf(3.) + (4. * x.powf(2.)) + (2. * x) - 10.;

    let t = record_execution_times(execs, || {
        let t0 = current_time_nanos();
        secant(f1, (2., 4.), tol, 100000, Absolute);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("secant".to_string(), t);

    let df1 = |x: f64| {
        return x * (8. - (3. * x) + (5. * x.powf(3.)));
    };

    let t = record_execution_times(execs, || {
        let t0 = current_time_nanos();
        newton(f1, df1, 2., tol, 100000, Absolute);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("newton".to_string(), t);

    let f2 = |x: f64| {
        return ((E.powf(x) - (5. * x) + 2.) / (2. * x)) * x.powf(3.);
    };

    let df2 = |x: f64| {
        return (0.5 * x) * (4. - (15. * x) + (E.powf(x) * (2. + x)));
    };

    let ddf2 = |x: f64| {
        return 2. - (15. * x) + ((0.5 * E.powf(x)) * (2. + (4. * x) + x.powf(2.)));
    };

    let t = record_execution_times(execs, || {
        let t0 = current_time_nanos();
        multiple_roots(f2, df2, ddf2, 2., tol, 100000, Absolute);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("multiple_roots".to_string(), t);

    let data_path =
        Path::new("/home/sebastian/university/numeric_methods/numeric_project/data/data.json");
    let data = read_data(&data_path);

    let t = record_execution_times(execs, || {
        let mut a = data.a0_spd.clone();
        let b = data.b0_spd.clone();

        let t0 = current_time_nanos();
        regressive_substitution(solve_by_simple_gaussian_elim(&mut a, &b).view(), None);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("gaussian_elim".to_string(), t);

    let t = record_execution_times(execs, || {
        let mut a = data.a0_spd.clone();
        let b = data.b0_spd.clone();

        let t0 = current_time_nanos();
        regressive_substitution(
            solve_by_gaussian_elim_with_partial_pivot(&mut a, &b).view(),
            None,
        );
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("gaussian_elim_with_partial_pivot".to_string(), t);

    let t = record_execution_times(execs, || {
        let mut a = data.a0_spd.clone();
        let b = data.b0_spd.clone();

        let t0 = current_time_nanos();
        let (ab, labels) = solve_by_gaussian_elim_with_total_pivot(&mut a, &b);
        regressive_substitution(ab.view(), Some(labels));
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("gaussian_elim_with_total_pivot".to_string(), t);

    let t = record_execution_times(execs, || {
        let mut a = data.a0_spd.clone();
        let b = data.b0_spd.clone();

        let t0 = current_time_nanos();
        solve_by_crout_fac(&mut a, &b);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("crout_fac".to_string(), t);

    let t = record_execution_times(execs, || {
        let mut a = data.a0_sdd.clone();
        let b = data.b0_sdd.clone();
        let init = data.b0_sdd.clone();

        let t0 = current_time_nanos();
        seidel_method(&mut a, b, init, tol, 100000, Absolute);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("seidel".to_string(), t);

    let t = record_execution_times(execs, || {
        let points = data.points0.clone();

        let t0 = current_time_nanos();
        newton_interpolation(&points);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("newton_interpolation".to_string(), t);

    let t = record_execution_times(execs, || {
        let mut points = data.points0.clone();

        let t0 = current_time_nanos();
        quadratic_spline(&mut points);
        let t1 = current_time_nanos();

        return t1 - t0;
    });
    results.insert("quadratic_splines".to_string(), t);

    let results_path = Path::new(
        "/home/sebastian/university/numeric_methods/numeric_project/data/rust-results.json",
    );
    let mut file = File::create(results_path).unwrap();
    let obj = serde_json::to_string(&results).unwrap();
    file.write_all(obj.as_bytes()).unwrap();
}
