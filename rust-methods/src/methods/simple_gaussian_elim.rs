use ndarray::Array2;

use crate::methods::matrix_utils::to_aug;

pub fn solve_by_simple_gaussian_elim(a: &mut Array2<f64>, b: &Array2<f64>) -> Array2<f64> {
    let n: usize = a.shape()[0];
    let mut ab = to_aug(a.view(), b.view());

    for k in 0..n - 1 {
        for i in k + 1..n {
            let multiplier = ab[[i, k]] / ab[[k, k]];
            for j in k..n + 1 {
                ab[[i, j]] = ab[[i, j]] - (multiplier * ab[[k, j]]);
            }
        }
    }

    return ab;
}

// WARNING: This function mutates input matrix a
pub fn simple_gaussian_elim(a: &mut Array2<f64>) -> Array2<f64> {
    let n: usize = a.shape()[0];
    let mut lower_tri: Array2<f64> = Array2::eye(n);

    for k in 0..n - 1 {
        for i in k + 1..n {
            let multiplier = a[[i, k]] / a[[k, k]];
            for j in k..n {
                a[[i, j]] = a[[i, j]] - (multiplier * a[[k, j]]);
                if i > j {
                    lower_tri[[i, j]] = multiplier;
                }
            }
        }
    }

    return lower_tri;
}
