use std::time::SystemTime;

pub fn record_execution_times<F>(n: usize, f: F) -> Vec<(usize, u128)>
where
    F: Fn() -> u128,
{
    let mut ts = Vec::new();

    // Discard first execution to avoid system variability.
    f();
    for i in 0..n {
        ts.push((i + 1, f()));
    }

    return ts;
}

pub fn average_exec_time(ts: &Vec<(usize, u128)>) -> f64 {
    // let sum: u32 = vec![1,2,3,4,5,6].iter().fold(0u32, |sum, val| sum += val);
    let tt = ts.iter().fold(0, |mut accum, (_, t)| {
        accum += t;
        accum
    });

    return tt as f64 / ts.len() as f64;
}

pub fn current_time_nanos() -> u128 {
    return SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_nanos();
}
