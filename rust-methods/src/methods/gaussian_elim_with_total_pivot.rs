use ndarray::Array2;

use crate::methods::matrix_utils::to_aug;
use crate::methods::total_pivot::total_pivot;

pub fn solve_by_gaussian_elim_with_total_pivot(
    a: &mut Array2<f64>,
    b: &Array2<f64>,
) -> (Array2<f64>, Vec<usize>) {
    let mut ab = to_aug(a.view(), b.view());
    let n: usize = a.shape()[0];
    let mut labels = (0..n).collect();

    for k in 0..n - 1 {
        total_pivot(ab.view_mut(), k, &mut labels);
        for i in k + 1..n {
            let multiplier = ab[[i, k]] / ab[[k, k]];
            for j in k..n + 1 {
                ab[[i, j]] = ab[[i, j]] - (multiplier * ab[[k, j]]);
            }
        }
    }

    return (ab, labels);
}
