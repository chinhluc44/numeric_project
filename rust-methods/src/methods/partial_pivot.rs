use ndarray::ArrayViewMut2;

use crate::methods::matrix_utils::swap;

// This procedure mutates the input matrix.
pub fn partial_pivot<'a>(mut ab: ArrayViewMut2<'a, f64>, k: usize) {
    let mut largest = ab[[k, k]].abs();
    let mut largest_row = k;
    let n = ab.shape()[0];

    for r in k + 1..n {
        let current = ab[[r, k]].abs();
        if current > largest {
            largest = current;
            largest_row = r;
        }
    }

    if largest == 0.0 {
        panic!("Equation system does not have unique solution.")
    } else {
        if largest_row != k {
            swap(&mut ab, k, largest_row, true);
        }
    }
}

pub fn partial_pivot_with_permutation<'a>(
    mut a: ArrayViewMut2<'a, f64>,
    mut lower_tri: ArrayViewMut2<'a, f64>,
    mut permutation: ArrayViewMut2<'a, f64>,
    k: usize,
) -> bool {
    let mut largest = a[[k, k]].abs();
    let mut largest_row = k;
    let n = a.shape()[0];

    for r in k + 1..n {
        let current = a[[r, k]].abs();
        if current > largest {
            largest = current;
            largest_row = r;
        }
    }

    if largest == 0.0 {
        panic!("Equation system does not have unique solution.")
    } else {
        if largest_row != k {
            swap(&mut a, k, largest_row, true);
            swap(&mut permutation, k, largest_row, true);

            if k > 0 {
                for s in 0..k {
                    let tmp = lower_tri[[largest_row, s]];
                    lower_tri[[largest_row, s]] = lower_tri[[k, s]];
                    lower_tri[[k, s]] = tmp;
                }
            }

            return true;
        } else {
            return false;
        }
    }
}
