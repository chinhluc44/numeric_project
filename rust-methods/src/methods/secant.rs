use crate::methods::error_type::ErrorType;
use crate::methods::error_type::ErrorType::{Fx, Relative};

pub fn secant<F>(f: F, interval: (f64, f64), tol: f64, n: u32, err_type: ErrorType) -> Option<f64>
where
    F: Fn(f64) -> f64,
{
    let mut x0 = interval.0;
    let mut x1 = interval.1;

    let mut i;
    let mut fx0 = f(x0);

    if fx0 == 0.0 {
        return Some(x0);
    } else {
        let mut abs_err;
        let mut rel_err;
        let mut error = f64::INFINITY;

        let mut fx1 = f(x1);

        if err_type == Fx {
            error = fx1.abs();
        }

        let mut denominator = fx1 - fx0;
        i = 1;

        let mut x2;
        while error >= tol && fx1 != 0.0 && denominator != 0.0 && i < n {
            x2 = x1 - ((fx1 * (x1 - x0)) / denominator);
            abs_err = (x2 - x1).abs();
            rel_err = abs_err / x2.abs();

            if err_type == Relative {
                error = rel_err;
            } else {
                error = abs_err;
            }

            x0 = x1;
            fx0 = fx1;
            x1 = x2;
            fx1 = f(x1);

            if err_type == Fx {
                error = fx1.abs();
            }

            denominator = fx1 - fx0;

            i += 1;
        }

        if fx1 == 0.0 || error < tol {
            return Some(x1);
        } else {
            return None;
        }
    }
}
