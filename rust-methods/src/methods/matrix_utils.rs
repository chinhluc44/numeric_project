use ndarray::{concatenate, Array2, ArrayView2, ArrayViewMut2, Axis, Zip};
use serde::Deserialize;
use std::fs;
use std::path::Path;

pub type Matrix = Array2<f64>;
pub type Vector = Array2<f64>;

pub fn to_aug<'a>(a: ArrayView2<'a, f64>, b: ArrayView2<'a, f64>) -> Matrix {
    return concatenate![Axis(1), a, b];
}

pub fn mk_vec<'a>(vs: Vec<f64>) -> Vector {
    return Array2::from_shape_vec((vs.len(), 1), vs).unwrap();
}

pub fn swap<'a>(a: &mut ArrayViewMut2<'a, f64>, i: usize, j: usize, rows: bool) {
    let axis = if rows { Axis(0) } else { Axis(1) };
    let mut it = a.axis_iter_mut(axis);
    let min_r = std::cmp::min(i, j);
    let max_r = std::cmp::max(i, j);
    let diff = max_r - (min_r + 1);
    let r0 = it.nth(min_r).unwrap();
    let r1 = it.nth(diff).unwrap();

    Zip::from(r0).and(r1).for_each(::std::mem::swap);
}

pub fn abs<'a>(mut a: ArrayViewMut2<'a, f64>) {
    for r in a.iter_mut() {
        *r = r.abs();
    }
}

pub fn max<'a>(a: ArrayView2<'a, f64>) -> f64 {
    let mut max_val = f64::NEG_INFINITY;

    for r in a.iter() {
        if max_val < *r {
            max_val = *r;
        }
    }

    return max_val;
}

#[derive(Deserialize, Clone)]
struct RawData {
    a0_spd: Vec<Vec<f64>>,
    b0_spd: Vec<f64>,
    a1_spd: Vec<Vec<f64>>,
    b1_spd: Vec<f64>,
    a2_spd: Vec<Vec<f64>>,
    b2_spd: Vec<f64>,
    a0_sdd: Vec<Vec<f64>>,
    b0_sdd: Vec<f64>,
    a1_sdd: Vec<Vec<f64>>,
    b1_sdd: Vec<f64>,
    a2_sdd: Vec<Vec<f64>>,
    b2_sdd: Vec<f64>,
    points0: Vec<(f64, f64)>,
    points1: Vec<(f64, f64)>,
    points2: Vec<(f64, f64)>,
}

pub struct Data {
    pub a0_spd: Matrix,
    pub b0_spd: Vector,
    pub a1_spd: Matrix,
    pub b1_spd: Vector,
    pub a2_spd: Matrix,
    pub b2_spd: Vector,
    pub a0_sdd: Matrix,
    pub b0_sdd: Vector,
    pub a1_sdd: Matrix,
    pub b1_sdd: Vector,
    pub a2_sdd: Matrix,
    pub b2_sdd: Vector,
    pub points0: Vec<(f64, f64)>,
    pub points1: Vec<(f64, f64)>,
    pub points2: Vec<(f64, f64)>,
}

fn read_rawdata(path: &Path) -> RawData {
    let data = fs::read(path).unwrap();
    return serde_json::from_slice(data.as_slice()).unwrap();
}

fn shape_independent_terms(vs: Vec<f64>) -> Vector {
    return Array2::from_shape_vec((vs.len(), 1), vs).unwrap();
}

fn shape_square_matrix(vss: Vec<Vec<f64>>) -> Matrix {
    let n = vss[0].len();
    return Array2::from_shape_vec((n, n), vss.concat()).unwrap();
}

pub fn read_data(path: &Path) -> Data {
    let rd = read_rawdata(path);
    return Data {
        a0_spd: shape_square_matrix(rd.a0_spd),
        b0_spd: shape_independent_terms(rd.b0_spd),
        a1_spd: shape_square_matrix(rd.a1_spd),
        b1_spd: shape_independent_terms(rd.b1_spd),
        a2_spd: shape_square_matrix(rd.a2_spd),
        b2_spd: shape_independent_terms(rd.b2_spd),
        a0_sdd: shape_square_matrix(rd.a0_sdd),
        b0_sdd: shape_independent_terms(rd.b0_sdd),
        a1_sdd: shape_square_matrix(rd.a1_sdd),
        b1_sdd: shape_independent_terms(rd.b1_sdd),
        a2_sdd: shape_square_matrix(rd.a2_sdd),
        b2_sdd: shape_independent_terms(rd.b2_sdd),
        points0: rd.points0,
        points1: rd.points1,
        points2: rd.points2,
    };
}
