use ndarray::Array2;

use crate::methods::matrix_utils::to_aug;
use crate::methods::partial_pivot::{partial_pivot, partial_pivot_with_permutation};

pub fn solve_by_gaussian_elim_with_partial_pivot(
    a: &mut Array2<f64>,
    b: &Array2<f64>,
) -> Array2<f64> {
    let mut ab = to_aug(a.view(), b.view());
    let n = a.shape()[0];

    for k in 0..n - 1 {
        partial_pivot(ab.view_mut(), k);
        for i in k + 1..n {
            let multiplier = ab[[i, k]] / ab[[k, k]];
            for j in k..n + 1 {
                ab[[i, j]] = ab[[i, j]] - (multiplier * ab[[k, j]]);
            }
        }
    }

    return ab;
}

pub fn lu_factorization_with_partial_pivot(
    a: &mut Array2<f64>,
) -> (Array2<f64>, Array2<f64>, usize) {
    let mut q = 0;
    let n = a.shape()[0];
    let mut permutation: Array2<f64> = Array2::eye(n);
    let mut lower_tri: Array2<f64> = Array2::eye(n);

    // Stages
    for k in 0..n - 1 {
        if partial_pivot_with_permutation(
            a.view_mut(),
            lower_tri.view_mut(),
            permutation.view_mut(),
            k,
        ) {
            q += 1;
        }
        for i in k + 1..n {
            let multiplier = a[[i, k]] / a[[k, k]];
            for j in k..n {
                a[[i, j]] -= multiplier * a[[k, j]];
                if i > j {
                    lower_tri[[i, j]] = multiplier;
                }
            }
        }
    }

    return (lower_tri, permutation, q);
}
