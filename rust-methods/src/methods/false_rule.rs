use crate::methods::error_type::ErrorType;
use crate::methods::error_type::ErrorType::{Absolute, Fx, Relative};

pub fn false_rule<F>(f: F, interval: (f64, f64), tol: f64, err_type: ErrorType) -> Option<f64>
where
    F: Fn(f64) -> f64,
{
    let mut xlo = interval.0;
    let mut xup = interval.1;
    let mut fxlo = f(xlo);
    let mut fxup = f(xup);

    if fxlo == 0.0 {
        return Some(xlo);
    }

    if fxup == 0.0 {
        return Some(xup);
    }

    if fxlo * fxup < 0.0 {
        let mut xm = xlo - ((fxlo * (xup - xlo)) / (fxup - fxlo));
        let mut fxm = f(xm);
        let mut abs_err;
        let mut rel_err;
        let mut error = f64::INFINITY;

        if err_type == Fx {
            error = fxm.abs();
        }

        while error >= tol && fxm != 0.0 {
            if fxlo * fxm < 0.0 {
                xup = xm;
                fxup = fxm;
            } else if fxm * fxup < 0.0 {
                xlo = xm;
                fxlo = fxm;
            }

            let xtemp = xm;
            xm = xlo - ((fxlo * (xup - xlo)) / (fxup - fxlo));
            fxm = f(xm);
            abs_err = (xm - xtemp).abs();
            rel_err = abs_err / xm.abs();

            match err_type {
                Absolute => {
                    error = abs_err;
                }
                Relative => {
                    error = rel_err;
                }
                Fx => {
                    error = fxm.abs();
                }
            }
        }

        if fxm == 0.0 || error < tol {
            return Some(xm);
        } else {
            return None;
        }
    } else {
        return None;
    }
}
