use ndarray::Array2;

pub fn cholesky_factorization(a: &mut Array2<f64>) -> (Array2<f64>, Array2<f64>) {
    let n = a.shape()[0];
    let mut lower_tri: Array2<f64> = Array2::eye(n);
    let mut upper_tri: Array2<f64> = Array2::eye(n);

    for k in 0..n {
        let mut sum0 = 0.0f64;
        // Compute lower_tri[[k, k]]
        for p in 0..k {
            sum0 += lower_tri[[k, p]] * upper_tri[[p, k]];
        }
        upper_tri[[k, k]] = (a[[k, k]] - sum0).sqrt();
        lower_tri[[k, k]] = upper_tri[[k, k]];

        // Compute lower_tri[[i, k]]
        for i in k + 1..n {
            let mut sum1 = 0.0;
            for p in 0..k {
                sum1 += lower_tri[[i, p]] * upper_tri[[p, k]];
            }
            lower_tri[[i, k]] = (a[[i, k]] - sum1) / lower_tri[[k, k]];
        }

        // Compute upper_tri[[k, j]]
        for j in k + 1..n {
            let mut sum2 = 0.0;
            for p in 0..k {
                sum2 += lower_tri[[k, p]] * upper_tri[[p, j]];
            }
            upper_tri[[k, j]] = (a[[k, j]] - sum2) / upper_tri[[k, k]];
        }
    }

    return (lower_tri, upper_tri);
}
