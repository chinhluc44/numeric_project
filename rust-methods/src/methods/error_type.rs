#[derive(PartialEq, Eq, Debug)]
pub enum ErrorType {
    Absolute,
    Relative,
    Fx,
}
