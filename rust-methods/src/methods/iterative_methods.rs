use ndarray::Array2;

use crate::methods::error_type::ErrorType;
use crate::methods::error_type::ErrorType::Relative;
use crate::methods::matrix_utils::{abs, max, Matrix, Vector};

pub fn jacobi_method(
    a: &mut Matrix,
    b: Vector,
    init: Vector,
    tol: f64,
    n: usize,
    err_type: ErrorType,
) -> Vector {
    let mut error = f64::INFINITY;
    let mut xn = init;
    let mut i = 0;

    while error > tol && i < n {
        let (x, abs_err, rel_err) = next_jacobi_iteration(a, &b, xn);
        xn = x;

        if err_type == Relative {
            error = rel_err;
        } else {
            error = abs_err;
        }

        i += 1;
    }

    return xn;
}

fn next_jacobi_iteration(a: &mut Matrix, b: &Vector, prev_x: Vector) -> (Vector, f64, f64) {
    let n = a.shape()[0];
    let mut x: Vector = Array2::zeros((n, 1));

    for i in 0..n {
        let mut accum = 0.0;
        for j in 0..n {
            if j != i {
                accum += a[[i, j]] * prev_x[[j, 0]];
            }
        }
        x[[i, 0]] = (b[[i, 0]] - accum) / a[[i, i]];
    }

    let mut errs = &x - &prev_x;
    abs(errs.view_mut());
    let abs_err = max(errs.view());

    let mut rel_errs = &errs / &x;
    abs(rel_errs.view_mut());
    let rel_err = max(rel_errs.view());

    return (x, abs_err, rel_err);
}

pub fn seidel_method(
    a: &mut Matrix,
    b: Vector,
    init: Vector,
    tol: f64,
    n: usize,
    err_type: ErrorType,
) -> Vector {
    let mut error = f64::INFINITY;
    let mut xn = init;
    let mut i = 0;

    while error > tol && i < n {
        let (x, abs_err, rel_err) = next_seidel_iteration(a, &b, xn);
        xn = x;

        if err_type == Relative {
            error = rel_err;
        } else {
            error = abs_err;
        }

        i += 1;
    }

    return xn;
}

fn next_seidel_iteration(a: &mut Matrix, b: &Vector, prev_x: Vector) -> (Vector, f64, f64) {
    let n = a.shape()[0];
    let mut x = prev_x.clone();

    for i in 0..n {
        let mut accum = 0.0;
        for j in 0..n {
            if j != i {
                accum += a[[i, j]] * x[[j, 0]];
            }
        }

        x[[i, 0]] = (b[[i, 0]] - accum) / a[[i, i]];
    }

    let mut errs = &x - &prev_x;
    abs(errs.view_mut());
    let abs_err = max(errs.view());

    let mut rel_errs = &errs / &x;
    abs(rel_errs.view_mut());
    let rel_err = max(rel_errs.view());

    return (x, abs_err, rel_err);
}
