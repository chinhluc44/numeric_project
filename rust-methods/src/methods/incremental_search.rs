pub fn incremental_search<F>(f: F, xinit: f64, dx: f64, n: u32) -> Option<(f64, f64)>
where
    F: Fn(f64) -> f64,
{
    let mut x0 = xinit;
    let mut fx0 = f(x0);

    if fx0 == 0.0 {
        return Some((x0, x0));
    } else {
        let mut x1 = x0 + dx;
        let mut fx1 = f(x1);

        for _ in 1..=n {
            x0 = x1;
            x1 = x0 + dx;

            fx0 = fx1;
            fx1 = f(x1);

            if fx1 == 0.0 {
                return Some((x1, x1));
            }

            if fx0 * fx1 < 0.0 {
                return Some((x0, x1));
            }
        }
    }

    return None;
}
