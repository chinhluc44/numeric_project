use ndarray::Array2;

use crate::methods::gaussian_elim_with_partial_pivot::solve_by_gaussian_elim_with_partial_pivot;
use crate::methods::regressive_substitution::regressive_substitution;

pub fn linear_spline<'a>(points: &mut Vec<(f64, f64)>) -> (Vec<(f64, f64, f64)>, Vec<(f64, f64)>) {
    points.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let mut equations: Vec<(f64, f64, f64)> = Vec::new();
    let mut intervals: Vec<(f64, f64)> = Vec::new();

    for (p0, p1) in points.iter().zip(points[1..].iter()) {
        let (x0, f0) = p0;
        let (x1, f1) = p1;
        let b = (f1 - f0) / (x1 - x0);
        intervals.push((*x0, *x1));
        equations.push((*f0, b, *x0));
    }

    return (equations, intervals);
}

pub fn quadratic_spline<'a>(points: &mut Vec<(f64, f64)>) -> (Vec<f64>, Vec<(f64, f64)>) {
    points.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let num_intervals = points.len() - 1;
    let dim = (num_intervals * 2) - 1;
    let mut hs = Vec::new();
    let mut zs = Array2::zeros((dim, 1));
    let mut system = Array2::zeros((dim, dim));
    let mut intervals = Vec::new();

    let mut col_shift = 0;
    let it = points.iter().zip(points[1..].iter());
    for (i, (p0, p1)) in it.enumerate() {
        let (x0, f0) = p0;
        let (x1, f1) = p1;

        intervals.push((*x0, *x1));
        let h = x1 - x0;

        system[[i, col_shift]] = h;
        if i == 0 {
            col_shift += 1;
        } else {
            system[[i, col_shift + 1]] = h.powf(2.0);
            col_shift += 2;
        }

        hs.push(h);
        zs[[i, 0]] = f1 - f0;
    }

    col_shift = 0;

    for i in num_intervals..dim {
        system[[i, col_shift]] = 1.0;

        if i == num_intervals {
            system[[i, col_shift + 1]] = -1.0;
            col_shift += 1;
        } else {
            system[[i, col_shift + 1]] = 2.0 * hs[i - num_intervals];
            system[[i, col_shift + 2]] = -1.0;
            col_shift += 2;
        }
    }

    let ab = solve_by_gaussian_elim_with_partial_pivot(&mut system, &zs);
    let sols = regressive_substitution(ab.view(), None);

    return (sols, intervals);
}
