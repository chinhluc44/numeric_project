use ndarray::ArrayViewMut2;

use crate::methods::matrix_utils::swap;

// This procedure mutates the input matrix.
pub fn total_pivot<'a>(mut ab: ArrayViewMut2<'a, f64>, k: usize, labels: &mut Vec<usize>) {
    let mut largest = ab[[k, k]].abs();
    let mut largest_row = k;
    let mut largest_col = k;
    let n = ab.shape()[0];

    for r in k..n {
        for c in k..n {
            let current = ab[[r, c]].abs();
            if current > largest {
                largest = current;
                largest_row = r;
                largest_col = c;
            }
        }
    }

    if largest == 0.0 {
        panic!("Equation system does not have unique solution.")
    } else {
        if largest_row != k {
            swap(&mut ab, k, largest_row, true);
        }

        if largest_col != k {
            swap(&mut ab, k, largest_col, false);
            let tmp = labels[k];
            labels[k] = labels[largest_col];
            labels[largest_col] = tmp;
        }
    }
}
