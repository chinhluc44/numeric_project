use ndarray::ArrayView2;

pub fn regressive_substitution<'a>(
    ab: ArrayView2<'a, f64>,
    maybe_labels: Option<Vec<usize>>,
) -> Vec<f64> {
    let n: usize = ab.shape()[0];
    let mut xs = vec![0.0; n];
    xs[n - 1] = ab[[n - 1, n]] / ab[[n - 1, n - 1]];

    for i in (0..=n - 2).rev() {
        let mut accum = 0.0;
        for p in i + 1..n {
            accum += ab[[i, p]] * xs[p];
        }
        xs[i] = (ab[[i, n]] - accum) / ab[[i, i]];
    }

    let mut labeled_xs = vec![0.0; n];
    match maybe_labels {
        Some(labels) => {
            for i in 0..labels.len() {
                labeled_xs[labels[i]] = xs[i];
            }

            return labeled_xs;
        }
        None => return xs,
    }
}
