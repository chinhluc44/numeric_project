use crate::methods::error_type::ErrorType;
use crate::methods::error_type::ErrorType::{Absolute, Fx, Relative};

pub fn newton<F, G>(f: F, df: G, x0: f64, tol: f64, n: u32, err_type: ErrorType) -> Option<f64>
where
    F: Fn(f64) -> f64,
    G: Fn(f64) -> f64,
{
    let mut x = x0;
    let mut fx = f(x);
    let mut dfx = df(x);
    let mut abs_err;
    let mut rel_err;
    let mut error = f64::INFINITY;

    if err_type == Fx {
        error = fx.abs();
    }

    let mut xn;
    let mut i = 0;
    while error >= tol && fx != 0.0 && i < n {
        xn = x - (fx / dfx);
        fx = f(xn);
        dfx = df(xn);

        abs_err = (xn - x).abs();
        rel_err = abs_err / xn.abs();

        match err_type {
            Absolute => {
                error = abs_err;
            }
            Relative => {
                error = rel_err;
            }
            Fx => {
                error = fx.abs();
            }
        }

        x = xn;
        i += 1;
    }

    if fx == 0.0 || error < tol {
        return Some(x);
    } else {
        return None;
    }
}
