use ndarray::Array2;

use crate::methods::cholesky_factorization::cholesky_factorization;
use crate::methods::crout_factorization::crout_factorization;
use crate::methods::dolittle_factorization::dolittle_factorization;
use crate::methods::gaussian_elim_with_partial_pivot::lu_factorization_with_partial_pivot;
use crate::methods::matrix_utils::{mk_vec, to_aug};
use crate::methods::progressive_substitution::progressive_substitution;
use crate::methods::regressive_substitution::regressive_substitution;
use crate::methods::simple_gaussian_elim::simple_gaussian_elim;

// WARNING: This function mutates input matrix a
pub fn solve_by_simple_gaussian_fac(a: &mut Array2<f64>, b: &Array2<f64>) -> Vec<f64> {
    let lower_tri = simple_gaussian_elim(a);
    let z = progressive_substitution(to_aug(lower_tri.view(), b.view()).view());
    let x = regressive_substitution(to_aug(a.view(), mk_vec(z).view()).view(), None);

    return x;
}

pub fn solve_by_lu_fac_with_partial_pivot(
    a: &mut Array2<f64>,
    b: &Array2<f64>,
) -> (Vec<f64>, usize) {
    let (lower_tri, permutation, q) = lu_factorization_with_partial_pivot(a);
    let pb = permutation.dot(b);
    let z = progressive_substitution(to_aug(lower_tri.view(), pb.view()).view());
    let x = regressive_substitution(to_aug(a.view(), mk_vec(z).view()).view(), None);

    return (x, q);
}

pub fn solve_by_crout_fac(a: &mut Array2<f64>, b: &Array2<f64>) -> Vec<f64> {
    let (lower_tri, upper_tri) = crout_factorization(a);
    let z = progressive_substitution(to_aug(lower_tri.view(), b.view()).view());
    let x = regressive_substitution(to_aug(upper_tri.view(), mk_vec(z).view()).view(), None);

    return x;
}

pub fn solve_by_cholesky_fac(a: &mut Array2<f64>, b: &Array2<f64>) -> Vec<f64> {
    let (lower_tri, upper_tri) = cholesky_factorization(a);
    let z = progressive_substitution(to_aug(lower_tri.view(), b.view()).view());
    let x = regressive_substitution(to_aug(upper_tri.view(), mk_vec(z).view()).view(), None);

    return x;
}

pub fn solve_by_dolittle_fac(a: &mut Array2<f64>, b: &Array2<f64>) -> Vec<f64> {
    let (lower_tri, upper_tri) = dolittle_factorization(a);
    let z = progressive_substitution(to_aug(lower_tri.view(), b.view()).view());
    let x = regressive_substitution(to_aug(upper_tri.view(), mk_vec(z).view()).view(), None);

    return x;
}
