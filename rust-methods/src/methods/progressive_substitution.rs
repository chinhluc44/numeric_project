use ndarray::ArrayView2;

pub fn progressive_substitution<'a>(ab: ArrayView2<'a, f64>) -> Vec<f64> {
    let n: usize = ab.shape()[0];
    let mut xs = vec![0.0; n];
    xs[0] = ab[[0, n]] / ab[[0, 0]];

    for i in 1..n {
        let mut accum = 0.0;
        for p in 0..i {
            accum += ab[[i, p]] * xs[p];
        }

        xs[i] = (ab[[i, n]] - accum) / ab[[i, i]];
    }

    return xs;
}
