pub fn newton_interpolation(points: &Vec<(f64, f64)>) -> Vec<f64> {
    if points.is_empty() {
        return Vec::new();
    } else {
        return interpolate(&points, points.len() - 1);
    }
}

fn interpolate(points: &Vec<(f64, f64)>, k: usize) -> Vec<f64> {
    let mut bs;
    if k == 0 {
        return vec![points[k].1];
    } else {
        let xn = points[k].0;
        bs = interpolate(points, k - 1);

        let mut denominator = 1.0;
        let mut numerator = 0.0;
        let m = bs.len() - 1;
        for (i, b) in bs.iter().enumerate() {
            let mut accum = 1.0;
            for j in 0..i {
                accum *= xn - points[j].0;
            }

            if i == m {
                denominator = (xn - points[k - 1].0) * accum;
            }

            numerator += b * accum;
        }

        let yn = points[k].1;
        bs.push((yn - numerator) / denominator);
    }

    return bs;
}
