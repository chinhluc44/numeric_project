# Execution time comparison for different numerical method algorithms between Rust and Python.

## Abstract

Python is one of the most popular programming languages in the academic realm specially because
of the extended usage of libraries such as `Tensor` Flow and `Keras` for machine learning and
data science. Nevertheless, being popular does not mean that it is the most suitable and performant
language to attack complex numerical problems that arise in research and industry. For this reason,
we have provided near-to-equivalent implementations of several types of numerical methods both in
Python and Rust in order to compare their performance over multiple executions. The results show that
there is a staggering performance gap between both languages with Rust being between 2 and 3 orders
of magnitude faster than Python. Therefore, we invite the readers to consider that a transition from
numerical kits written in Python to ones implemented in Rust might be beneficial both in industry and
research.


## Introduction

Python is one of the most popular programming languages in the academic realm specially because
of the extended usage of libraries such as `Tensor` Flow and `Keras` when prototyping and developing
models for machine learning. Moreover, even for less specialized numerical applications, `Numpy` has
become the default tool for teaching numerical algorithms [3]. Nonetheless, being the most popular
programming language does not make it the most efficient, safe and convenient tool for implementing
this type of algorithms. Python is a dynamically typed and garbage collected language. This means that
all of the type checking and disposal of unwarranted resources has to be done at execution time [4].
Moreover, Python has an execution model that involves "just-in-time-compilation" which means that
parts of the code will be translated into byte-code at execution time. Subsequently, this byte-code is
not executed directly by the machine's hardware, but it is interpreted by a virtual machine [4].

On the other hand, Rust is a programming language designed for performance and safety, specially in
contexts where high concurrency is involved. More specifically, we are interested in its performance
features which are powered by a series of traits, namely, it is a statically typed language which
means that all of the type and error checking happens at compilation time before execution [5]. Also,
it is not garbage collected which prevents unnecessary resource management by expensive run-time
programs. Nevertheless, the lack of garbage collection is compensated by its strong type system and
compiler that is capable of checking the scope and lifetime of resources at compilation time thus
guaranteeing optimal resource management, and preventing memory leaks [5]. All of these traits ensure
that a Rust binary chunk is as efficient as possible and ready to be executed in bare metal.

In this paper, we aim at showing the differences in performance between Rust and Python by providing
a benchmark of several numerical methods studied throughout the course with Professor Arenas V. Among
the methods that were selected in the course, we decided to do the benchmark with only a subset of
them due to the similarities between some of them, and also because some were not CPU intensive enough
to be interesting. The following root finding methods were selected:

- False rule method.
- Secant method.
- Newton's method.
- Multiple roots method.

Secondly, the following methods for solving systems of linear equations were chosen:

- Gaussian elimination with regressive substitution.
- Gaussian elimination with partial pivot and regressive substitution.
- Gaussian elimination with total pivot and regressive substitution.
- Solver with Cholesky's factorization and progressive and regressive substitution steps.
- Seidel's iterative method.

Finally, we have methods for interpolation and splines:

- Newton's interpolation (interpolating polynomial).
- Quadratic splines. Gaussian elimination with partial pivot and regressive substitution was used as solver.

A pseudo-code outline for the implementations of all methods except for quadratic splines can be
found in Correa's book "Métodos Numéricos" [6]. The methods for quadratic splines can be found in
Chapra's book "Numerical Methods for Engineers" 4th edition [7].

## Implementation considerations

In order to guarantee fairness on testing performance, each numerical method was implemented with the same algorithmic
structure in both languages. This means that the same number of steps was codified in both scenarios and the amount of
checks (`if` statements) and function calls was reduced as much as possible for both implementations. Due to the matrix
nature of those methods that involved solving linear equations, we resorted to only two external linear algebra libraries,
namely, `Numpy` and `ndarray`, for Python and Rust respectively. There is a heads-up regarding the nature of the `Numpy`
library, that is, its core is fundamentally written in C language to guarantee performance [3]. This means that the heavy
lifting for array-intensive operations will be done by C dynamic libraries that are accessed by Python's run-time via a
C foreign function interface (FFI). On the contrary, `ndarray` is a library implemented 100% in Rust, which means that
for matrix operations the comparison will be more between C and Rust [8][9]. However, the fact that `Numpy` has to use
a FFI to access C methods means that there will be an overhead to invoke those function calls, and dynamically external
data structures at run-time.

The fact that the core of `Numpy` is written in C should not be considered a liability when comparing the performance of
both language first because Python is generally much slower than C, and also because `Numpy` is so used and popular, that
it has become and indispensable tool in Python's ecosystem.

## Experiment design

There were three types of considerations taken into account to design the benchmarks, that is, 1) isolation of algorithm
execution, 2) numerical problems to be solved, and 3) execution environment. First, the isolation consideration has to do
with the fact that each algorithm must be executed right after an initial time (t0) is recorded in nanoseconds. We guaranteed
that during its execution there was no other interfering process--like function calls, parameter initialization, printing,
etc.--that could slow it down. Right after the algorithm finished, a final time (t1) was also recorded in nanoseconds,
and the duration of the execution was the delta between both times (t1 - t0). Each algorithm was executed 55 times and
the duration for each execution was stored in JSON format.

Secondly, for numerical methods involving root finding an absolute error tolerance of `0.5 * (10^-12)` was expected for
each execution and the following problems where chosen:

- False rule: cos(x / 10) with interval [460, 510].
- Secant: `(x^5) - (x^3) + (4 * (x^2)) + (2 * x) - 10` with interval `[2, 4]`.
- Newton: `x * (8 - (3 * x) + (5 * x ^ 3))` starting at `2`.
- Multiple roots: `((e^x) - (5 * x) + 2) / (2 * x)) * x^3` starting at `2`.

For linear equation systems two types of matrices were generated, namely, positive definite (PD) matrices and
strictly diagonally dominant (SDD) ones. The first type (PD) was required specifically for the solver with Cholesky
factorization because it guarantees that the problem is solvable via that method [6]. Nonetheless, PD matrices were also used
for methods based on Gaussian elimination. On the other hand, an SDD matrix guarantees that Seidel's method will converge
[6] and the one generated for this experiment has a spectral radius of 0.04766. PD matrices were generated via Python's
library `sklearn` which provides a data set for generating them [10]. On the order hand, SDD matrices were generated via
several steps:

- A square matrix was filled with uniformly distributed random floating point numbers.
- All of the elements of each row, except for its main diagonal, were added in absolute value.
- The resultant summation for each row was added to the absolute value of the corresponding main diagonal component.
  This new number was then assigned as the new diagonal component.

Both types of matrices had 100 by 100 rows and columns.

The set of points for interpolation and splines was generated by matching the range of natural numbers from
0 to 50 with uniformly distributed random integers within the range [1, 200].

Finally, regarding the execution environment, all methods were executed in an Ubuntu 20.04 operating system running on
a machine with an `Intel(R) Core(TM) i9-9900K CPU @ 3.60GHz` processor and 64GB of `DIMM DDR4 Synchronous 2133 MHz`
memory. The version of Python was `3.8.10` and the version of the Rust compiler was `1.60.0`. The Python code was executed
in a virtual environment isolated from all system conflicting dependencies, and the Rust code was compiled with release
optimizations.


## Results

The following graphs display the execution times for each one of the methods. The average execution time is also provided:

*1) False rule*

![false rule](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/false_rule/false_rule.png)

```
Rust average execution time: 21.05454 ns
Python average execution time: 2994.18181 ns

```

*2) Secant*

![secant](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/secant/secant.png)

```
Rust average excution time: 20.76363 ns
Python average execution time: 6676.63636 ns
```


*3) Newton*

![newton](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/newton/newton.png)

```
Rust average execution time: 20.38181 ns
Python average execution time: 10500.92727 ns
```


*4) Multiple roots*

![multiple roots](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/multiple_roots/multiple_roots.png)

```
Rust average execution time: 20.41818 ns
Python average execution time: 8540.01818 ns
```

*5) Solver with Gaussian elimination and regressive substitution*

![gaussian elim plot](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/gaussian_elimination/gaussian_elimination.png)

```
Rust average execution time: 181069.85454 ns
Python average execution time: 162894638.43636 ns
```

*6) Solver with Gaussian elimination/partial pivot and regressive substitution*

![gaussian partial pivot elim plot](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/gaussian_elim_with_partial_pivot/gaussian_elim_with_partial_pivot.png)

```
Rust average execution time: 182943.29090 ns
Python average execution time: 163285427.03636 ns
```

*7) Solver with Gaussian elimination/total pivot and regressive substitution*

![gaussian total pivot elim plot](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/gaussian_elim_with_total_pivot/gaussian_elim_with_total_pivot.png)

```
Rust average execution time: 616304.563636 ns
Python average execution time: 224566083.96363 ns
```

*8) Crout factorization*

![crout fac plot](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/crout_fac/crout_fac.png)

```
Rust average execution time: 238666.38181 ns
Python average execution time: 120573001.07272 ns
```

*9) Seidel's iterative method*

![seidel plot](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/seidel/seidel.png)

```
Rust average execution time: 115193.96363 ns
Python average execution time: 30541949.12727 ns
```

*10) Newton interpolation*

![newton interpolation plot](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/newton_interpolation/newton_interpolation.png)

```
Rust average execution time: 14387.85454 ns
Python average execution time: 1415580.34545 ns
```

*11) Quadratic splines*

![quadratic splines plot](https://gitlab.com/sebashack/numeric_project/-/raw/main/article-assets/quadratic_splines/quadratic_splines.png)

```
Rust average execution time: 174473.09090 ns
Python average execution time: 148755344.03636 ns
```

## Discussion and conclusions

Overall, Rust implementations had an overwhelming faster performance than Python's. Among the root
finding methods, the worst performance for Python was Newton's method where it was 3 orders of magnitude
slower than Rust. Almost all methods for solving linear systems were 3 orders of magnitude slower in
Python than in Rust except for Seidel's method which performed considerable better for both implementations,
which is expected of an iterative method. Nevertheless, it was still 2 orders of magnitude slower in Python
than in Rust. Newton interpolation and quadratic splines where 2 and 3 orders of magnitude respectively
slower in Python. Another characteristic that stands out is the steady values between executions
in Rust compared to the unstable execution times in Python. For instance, for `false rule` the execution
times start fairly large--close to 4000 ns--and then decrease until they become closer to 3000 ns.
There are also cases such as Gaussian elimination and Crout factorization where there are sudden spikes
in time execution for Python. This instability in Python's run-time could be explained by the fact that
it has to constantly make calls to C foreign functions, and transition between its data structures and C's.
Another reason could be that garbage collection might be taking place at some point between executions.

Given this staggering gap between Python's and Rust's performance, it could be appropriate to reformulate
Python's role in data science and numerical algorithms. This language's role might be well-suited for
pedagogical purposes, but its performance is by no means convenient for research and industry where
the complexity of problems might be far-reaching. When problems become too big, the tools we use to
attack them should be as performant as possible in order to minimize the time and energy that is spent
on computations. Another implication of bad performance is that it might not convey the best user experience
for some scenarios where real time processing required. For example, executing programs that apply
image processing algorithms--e.g. convolutions over a matrix--in a surveillance camera with hardware
limitations requires that the software avoids as much overhead as possible. Thus, we propose that popular
machine learning and data science kits that are implemented in Python are reimplemented in Rust's
ecosystem so that academy and industry enjoy its performance advantages.
