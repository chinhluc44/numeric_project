def record_execution_times(n, f):
    assert n > 0
    ts = []

    # Discard first execution to avoid system variabilities
    f()
    for i in range(0, n):
        ts.append((i + 1, f()))

    return ts


def average_exec_time(ts):
    accum = 0
    for i, t in ts:
        accum += t

    return accum / len(ts)
