import math

from constants import ABSOLUTE, RELATIVE, FX


def multiple_roots(f, df, ddf, x0, tol, n, err_type=ABSOLUTE):
    x = x0
    fx = f(x)
    dfx = df(x)
    ddfx = ddf(x)

    abs_err = float("inf")
    rel_err = float("inf")
    error = float("inf")

    if err_type == FX:
        error = abs(fx)

    i = 0
    while fx != 0 and error > tol and i < n:
        xn = x - ((fx * dfx) / (math.pow(dfx, 2) - (fx * ddfx)))
        fx = f(xn)
        dfx = df(xn)
        ddfx = ddf(xn)

        abs_err = abs(xn - x)
        rel_err = abs_err / abs(xn)

        if err_type == FX:
            error = abs(fx)
        elif err_type == RELATIVE:
            error = rel_err
        else:
            error = abs_err

        x = xn
        i += 1

    if fx == 0 or error < tol:
        return x
    else:
        return None
