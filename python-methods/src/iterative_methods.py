import numpy as np
from constants import ABSOLUTE, RELATIVE


def jacobi_method(a, b, init, tol, n, err_type=ABSOLUTE):
    error = float("inf")
    xn = init
    i = 0

    while error > tol and i < n:
        x, abs_err, rel_err = next_jacobi_iteration(a, b, xn)
        xn = x

        if err_type == RELATIVE:
            error = rel_err
        else:
            error = abs_err

        i += 1
    return xn


def next_jacobi_iteration(a, b, prev_x):
    n = a.shape[0]
    x = np.zeros(n, dtype=np.float64)

    for i in range(0, n):
        accum = 0
        for j in range(0, n):
            if j != i:
                accum += a[i][j] * prev_x[j]
        x[i] = (b[i] - accum) / a[i][i]

    errs = abs(x - prev_x)
    abs_err = max(errs)
    rel_err = max(errs / abs(x))

    return (x, abs_err, rel_err)


def seidel_method(a, b, init, tol, n, err_type=ABSOLUTE):
    error = float("inf")
    xn = init
    i = 0

    while error > tol and i < n:
        x, abs_err, rel_err = next_seidel_iteration(a, b, xn)
        xn = x

        if err_type == RELATIVE:
            error = rel_err
        else:
            error = abs_err

        i += 1

    return xn


def next_seidel_iteration(a, b, prev_x):
    n = a.shape[0]
    x = np.copy(prev_x).astype(np.float64)

    for i in range(0, n):
        accum = 0
        for j in range(0, n):
            if j != i:
                accum += a[i][j] * x[j]
        x[i] = (b[i] - accum) / a[i][i]

    errs = abs(x - prev_x)
    abs_err = max(errs)
    rel_err = max(errs / abs(x))

    return (x, abs_err, rel_err)
