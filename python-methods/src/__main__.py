import sys
import math
from time import time_ns

from plot import plot
from matrix_utils import gen_data, write_data, read_data, read_results
from false_rule import false_rule
from secant import secant
from newton import newton
from multiple_roots import multiple_roots


from newton_interpolation import newton_interpolation
from simple_gaussian_elim import solve_by_simple_gaussian_elim
from gaussian_elim_with_partial_pivot import solve_by_gaussian_elim_with_partial_pivot
from gaussian_elim_with_total_pivot import solve_by_gaussian_elim_with_total_pivot
from regressive_substitution import regressive_substitution
from iterative_methods_mat_form import seidel_spectral_radius
from iterative_methods import seidel_method
from splines import quadratic_splines
from factorization_solvers import solve_by_crout_fac
from constants import ABSOLUTE
from benchmark import record_execution_times, average_exec_time


def main(argv):
    execs = 55
    results = {}
    tol = 0.5 * math.pow(10, -12)

    def f0(x):
        return math.cos(x / 10)

    def false_rule_exec():
        t0 = time_ns()
        false_rule(f0, (460, 510), tol, err_type=ABSOLUTE)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, false_rule_exec)
    results["false_rule"] = ts

    def f1(x):
        return math.pow(x, 5) - math.pow(x, 3) + (4 * math.pow(x, 2)) + (2 * x) - 10

    def secant_exec():
        t0 = time_ns()
        secant(f1, (2, 4), tol, 100000, err_type=ABSOLUTE)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, secant_exec)
    results["secant"] = ts

    def df1(x):
        return x * (8 - (3 * x) + (5 * math.pow(x, 3)))

    def newton_exec():
        t0 = time_ns()
        newton(f1, df1, 2, tol, 100000, err_type=ABSOLUTE)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, newton_exec)
    results["newton"] = ts

    def f2(x):
        return ((math.pow(math.e, x) - (5 * x) + 2) / (2 * x)) * math.pow(x, 3)

    def df2(x):
        return (0.5 * x) * (4 - (15 * x) + (math.pow(math.e, x) * (2 + x)))

    def ddf2(x):
        return (
            2
            - (15 * x)
            + ((0.5 * math.pow(math.e, x)) * (2 + (4 * x) + math.pow(x, 2)))
        )

    def multiple_roots_exec():
        t0 = time_ns()
        multiple_roots(f2, df2, ddf2, 2, tol, 100000, err_type=ABSOLUTE)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, multiple_roots_exec)
    results["multiple_roots"] = ts

    path = "/home/sebastian/university/numeric_methods/numeric_project/data/data.json"
    # data_w = gen_data(100, 50)
    # write_data(path, data_w)
    data_r = read_data(path)

    def sgs_exec():
        a = data_r["a0_spd"].copy()
        b = data_r["b0_spd"].copy()

        t0 = time_ns()
        regressive_substitution(solve_by_simple_gaussian_elim(a, b))
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, sgs_exec)
    results["gaussian_elim"] = ts

    def gpp_exec():
        a = data_r["a0_spd"].copy()
        b = data_r["b0_spd"].copy()

        t0 = time_ns()
        regressive_substitution(solve_by_gaussian_elim_with_partial_pivot(a, b))
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, gpp_exec)
    results["gaussian_elim_with_partial_pivot"] = ts

    def gpt_exec():
        a = data_r["a0_spd"].copy()
        b = data_r["b0_spd"].copy()

        t0 = time_ns()
        ab, labels = solve_by_gaussian_elim_with_total_pivot(a, b)
        regressive_substitution(ab, labels)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, gpt_exec)
    results["gaussian_elim_with_total_pivot"] = ts

    def crout_exec():
        a = data_r["a0_spd"].copy()
        b = data_r["b0_spd"].copy()

        t0 = time_ns()
        solve_by_crout_fac(a, b)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, crout_exec)
    results["crout_fac"] = ts

    def seidel_exec():
        a = data_r["a0_sdd"].copy()
        b = data_r["b0_sdd"].copy()

        t0 = time_ns()
        seidel_method(a, b, b, tol, 100000, err_type=ABSOLUTE)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, seidel_exec)
    results["seidel"] = ts

    def ntinp():
        points = data_r["points0"].copy()

        t0 = time_ns()
        newton_interpolation(points)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, ntinp)
    results["newton_interpolation"] = ts

    def slnq():
        points = data_r["points1"].copy()

        t0 = time_ns()
        quadratic_splines(points)
        t1 = time_ns()

        return t1 - t0

    ts = record_execution_times(execs, slnq)
    results["quadratic_splines"] = ts

    # rs_results_path = (
    #     "/home/sebastian/university/numeric_methods/numeric_project/data/rust-results.json"
    # )
    py_results_path = "/home/sebastian/university/numeric_methods/numeric_project/data/python-results.json"
    write_data(py_results_path, results)

    # rs_results = read_results(rs_results_path)
    # py_results = read_results(py_results_path)

    # plot(
    #     "Solver with crout factorization",
    #     py_results["crout_fac"],
    #     rs_results["crout_fac"],
    # )


if __name__ == "__main__":
    main(sys.argv[2:])
