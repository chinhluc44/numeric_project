from gaussian_elim_with_partial_pivot import lu_factorization_with_partial_pivot
from matrix_utils import to_aug
from progressive_substitution import progressive_substitution
from regressive_substitution import regressive_substitution
from simple_gaussian_elim import simple_gaussian_elim
from cholesky_factorization import cholesky_factorization
from crout_factorization import crout_factorization
from dolittle_factorization import dolittle_factorization


# WARNING: This function mutates input matrix a
def solve_by_simple_gaussian_fac(a, b):
    lower_tri = simple_gaussian_elim(a)
    z = progressive_substitution(to_aug(lower_tri, b))
    x = regressive_substitution(to_aug(a, z))

    return x


# WARNING: This function mutates input matrix a
def solve_by_lu_fac_with_partial_pivot(a, b):
    lower_tri, permutation, q = lu_factorization_with_partial_pivot(a)
    pb = permutation.dot(b)
    z = progressive_substitution(to_aug(lower_tri, pb))
    x = regressive_substitution(to_aug(a, z))

    return (x, q)


def solve_by_crout_fac(a, b):
    lower_tri, upper_tri = crout_factorization(a)

    z = progressive_substitution(to_aug(lower_tri, b))
    x = regressive_substitution(to_aug(upper_tri, z))

    return x


def solve_by_dolittle_fac(a, b):
    lower_tri, upper_tri = dolittle_factorization(a)

    z = progressive_substitution(to_aug(lower_tri, b))
    x = regressive_substitution(to_aug(upper_tri, z))

    return x


def solve_by_cholesky_fac(a, b):
    lower_tri, upper_tri = cholesky_factorization(a)
    z = progressive_substitution(to_aug(lower_tri, b))
    x = regressive_substitution(to_aug(upper_tri, z))

    return x
