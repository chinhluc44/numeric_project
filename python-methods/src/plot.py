import matplotlib.pyplot as plt


def plot(title, py_points, rs_points):
    fig, xy = plt.subplots()

    pyks = []
    pyts = []

    rsks = []
    rsts = []

    for (pyk, pyt), (rsk, rst) in zip(py_points, rs_points):
        pyks.append(pyk)
        pyts.append(pyt)

        rsks.append(rsk)
        rsts.append(rst)

    (line,) = xy.plot(pyks, pyts, "b")
    line.set_label("Python")
    xy.legend()

    (line,) = xy.plot(rsks, rsts, "r")
    line.set_label("Rust")
    xy.legend()

    plt.xlabel("exec #")
    plt.ylabel("nanoseconds")
    plt.title(title)
    plt.show()
