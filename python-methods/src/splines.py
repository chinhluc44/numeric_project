import numpy as np
from collections import namedtuple

from gaussian_elim_with_partial_pivot import solve_by_gaussian_elim_with_partial_pivot
from regressive_substitution import regressive_substitution


Si = namedtuple("Si", "ai bi xi")


def linear_splines(points):
    points_ = sorted(points)
    equations = []
    intervals = []
    for p0, p1 in zip(points_, points_[1:]):
        x0, f0 = p0
        x1, f1 = p1
        b = (f1 - f0) / (x1 - x0)
        intervals.append((x0, x1))
        equations.append(Si(f0, b, x0))
    return (equations, intervals)


def quadratic_splines(points):
    points_ = sorted(points)
    num_intervals = len(points_) - 1
    dim = (num_intervals * 2) - 1
    hs = []
    zs = np.zeros(dim, dtype=np.float64)
    system = np.zeros((dim, dim), dtype=np.float64)
    intervals = []

    col_shift = 0
    for i, (p0, p1) in enumerate(zip(points_, points_[1:])):
        x0, f0 = p0
        x1, f1 = p1
        intervals.append((x0, x1))
        h = x1 - x0

        system[i][col_shift] = h
        if i == 0:
            col_shift += 1
        else:
            system[i][col_shift + 1] = np.power(h, 2)
            col_shift += 2

        hs.append(h)
        zs[i] = f1 - f0

    col_shift = 0
    for i in range(num_intervals, dim):
        system[i][col_shift] = 1
        if i == num_intervals:
            system[i][col_shift + 1] = -1
            col_shift += 1
        else:
            system[i][col_shift + 1] = 2 * hs[i - num_intervals]
            system[i][col_shift + 2] = -1
            col_shift += 2

    ab = solve_by_gaussian_elim_with_partial_pivot(system, zs)
    sols = regressive_substitution(ab)

    return (sols, intervals)
