import sympy as sym


def lagrange_polynomial(points):
    n = len(points)
    x = sym.Symbol("x")
    polynomial = 0

    for i in range(0, n, 1):
        numerator = 1
        denominator = 1
        for j in range(0, n, 1):
            if j != i:
                numerator = numerator * (x - points[j][0])
                denominator = denominator * (points[i][0] - points[j][0])
        termLi = numerator / denominator

        polynomial = polynomial + termLi * points[i][1]

    return str(polynomial.expand())
