from constants import ABSOLUTE, RELATIVE, FX


def false_rule(f, interval, tol, err_type=ABSOLUTE):
    xlo = interval[0]
    xup = interval[1]
    fxlo = f(xlo)
    fxup = f(xup)

    if fxlo == 0:
        return xlo
    elif fxup == 0:
        return xup
    elif fxlo * fxup < 0:
        xm = xlo - ((fxlo * (xup - xlo)) / (fxup - fxlo))
        fxm = f(xm)

        abs_err = float("inf")
        rel_err = float("inf")
        error = float("inf")

        if err_type == FX:
            error = abs(fxm)

        while error >= tol and fxm != 0:
            if fxlo * fxm < 0:
                xup = xm
                fxup = fxm
            elif fxm * fxup < 0:
                xlo = xm
                fxlo = fxm

            xtemp = xm
            xm = xlo - ((fxlo * (xup - xlo)) / (fxup - fxlo))
            fxm = f(xm)
            abs_err = abs(xm - xtemp)
            rel_err = abs_err / abs(xm)

            if err_type == FX:
                error = abs(fxm)
            elif err_type == RELATIVE:
                error = rel_err
            else:
                error = abs_err

        if fxm == 0 or error < tol:
            return xm
    else:
        return None
