import numpy as np
import random
from sklearn.datasets import make_spd_matrix
import json


def set_print_opts(decimal_places):
    np.set_printoptions(suppress=True)
    np.set_printoptions(precision=decimal_places)


def to_aug(a, b):
    return np.column_stack((a, b))


def mk_mat(lss):
    return np.array(lss).astype(np.float64)


def copy(m):
    return np.copy(m)


def mk_vec(vec):
    return np.array(vec).astype(np.float64)


def print_solution(vec):
    for i, x in enumerate(vec):
        print(f"x{i+1} = {x}")


def gen_spd_mat(n):
    seed = random.randint(0, 99999)
    return make_spd_matrix(n, random_state=seed)


def gen_sdd_mat(n):
    a = np.random.uniform(-7, 7, (n, n))
    a_ = a.copy()
    np.fill_diagonal(a_, 0.0)

    def f(vec):
        return sum(abs(vec))

    diag = np.apply_along_axis(f, axis=1, arr=a_)

    for i in range(0, n):
        for j in range(0, n):
            if i == j:
                a[i][j] = abs(a[i][j]) + diag[i]

    return a


def gen_vec(n):
    return np.random.uniform(-7, 7, n)


def gen_points(n):
    xs = list(range(0, n))
    random.shuffle(xs)
    ys = np.random.uniform(1, 200, n).tolist()

    return list(zip(xs, ys))


def gen_data(n, k):
    data = {
        "a0_spd": gen_spd_mat(n).tolist(),
        "b0_spd": gen_vec(n).tolist(),
        "a1_spd": gen_spd_mat(n).tolist(),
        "b1_spd": gen_vec(n).tolist(),
        "a2_spd": gen_spd_mat(n).tolist(),
        "b2_spd": gen_vec(n).tolist(),
        "a0_sdd": gen_sdd_mat(n).tolist(),
        "b0_sdd": gen_vec(n).tolist(),
        "a1_sdd": gen_sdd_mat(n).tolist(),
        "b1_sdd": gen_vec(n).tolist(),
        "a2_sdd": gen_sdd_mat(n).tolist(),
        "b2_sdd": gen_vec(n).tolist(),
        "points0": gen_points(k),
        "points1": gen_points(k),
        "points2": gen_points(k),
    }

    return data


def to_tuple(ls):
    return (ls[0], ls[1])


def write_data(path, obj):
    with open(path, "w") as outfile:
        json.dump(obj, outfile)


def read_data(path):
    with open(path) as json_file:
        obj = json.load(json_file)
        return {
            "a0_spd": mk_mat(obj["a0_spd"]),
            "b0_spd": mk_vec(obj["b0_spd"]),
            "a1_spd": mk_mat(obj["a1_spd"]),
            "b1_spd": mk_vec(obj["b1_spd"]),
            "a2_spd": mk_mat(obj["a2_spd"]),
            "b2_spd": mk_vec(obj["b2_spd"]),
            "a0_sdd": mk_mat(obj["a0_sdd"]),
            "b0_sdd": mk_vec(obj["b0_sdd"]),
            "a1_sdd": mk_mat(obj["a1_sdd"]),
            "b1_sdd": mk_vec(obj["b1_sdd"]),
            "a2_sdd": mk_mat(obj["a2_sdd"]),
            "b2_sdd": mk_vec(obj["b2_sdd"]),
            "points0": list(map(to_tuple, obj["points0"])),
            "points1": list(map(to_tuple, obj["points1"])),
            "points2": list(map(to_tuple, obj["points2"])),
        }


def read_results(path):
    with open(path) as json_file:
        return json.load(json_file)
