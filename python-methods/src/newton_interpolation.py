import numpy as np


# Points are (xn,yn) tuples
def newton_interpolation(points):
    if len(points) == 0:
        return []
    else:
        return interpolate(points, len(points) - 1)


def interpolate(points, k):
    bs = None

    if k == 0:
        return [points[k][1]]
    else:
        xn = points[k][0]
        bs = interpolate(points, k - 1)

        denominator = 1
        numerator = 0
        m = len(bs) - 1
        for i, b in enumerate(bs):
            accum = 1
            for j in range(0, i):
                accum *= xn - points[j][0]

            if i == m:
                denominator = (xn - points[k - 1][0]) * accum

            numerator += b * accum

        yn = points[k][1]

        bs.append((yn - numerator) / denominator)

    return bs


def newton_interpolation_by_diffs(points):
    n = len(points)
    table = np.zeros((n, n + 1), dtype=np.float64)

    for i, p in enumerate(points):
        table[i][0] = points[i][0]
        table[i][1] = points[i][1]

    bs = [table[0][1]]
    rows = table.shape[0]

    c = 0
    for j in range(2, n + 1):
        for i in range(j - 1, rows):
            num = table[i - 1][j - 1] - table[i][j - 1]
            denom = table[i - (1 + c)][j - (2 + c)] - table[i][j - (2 + c)]
            r = num / denom
            table[i][j] = r
            if j == i + 1:
                bs.append(r)

        c += 1

    return (table, bs)
